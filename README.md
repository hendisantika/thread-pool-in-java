# Thread Sample

## Thread Pool

#### Introduction

A Thread pool is a pattern to achieve the concurrency of execution in a computer program. A thread pool maintains multiple threads waiting for tasks to be allocated for concurrent execution by the supervising program.

By maintaining a pool of threads, the model increases performance and avoids latency in execution due to frequent creation and destruction of threads for short-lived tasks.

#### Why to use Thread Pool?

This is the main question which everyone can raise that why to use Thread Pool if there is already a concept to achieve concurrency which is initializing new threads each time and perform some task in each thread and destroy them whenever required.

Since a thread can only run once, you’d have to use a thread per task. However, creating and starting threads is somewhat expensive and can lead to a situation where too many threads are waiting for the executions and it can lead to overhead of the memory which can reduce the performance of the program. Thread objects use a significant amount of memory, and in a large-scale application, allocating and deallocating many thread objects creates a significant memory management overhead because switching between threads perform following things internally:

1. The thread is interrupted, the context of the CPU switched from user mode to kernel mode.
2. CPU context is saved, the CPU context for the new thread to be scheduled is loaded.
3. Then the context is switched from kernel mode to user mode.

This is quite a lot of work. When you have multiple threads starving for attention, it’s very hard to optimize the scheduler for such a situation, and to schedule fairly.

On the other hand, a thread pool is a pool of working threads which are always running. There is no need to initialize and destroy threads again and again. They just pickup from the pool and perform their task and if there is no task then thread will wait. So thread pool minimizes the overhead due to thread creation.

#### Pros of using Thread Pool:

1. No need to create new thread per task so it will save the work which is done when a new thread is initialized.
2. It helps in tracking the status of thread, exception handling, task cancellation, progress reporting of the task.

#### Cons of using Thread Pool:

    Thread Pools threads are not suitable for long running operation because it can lead to thread starvation.

#### When to use Thread Pool?

Thread Pools are useful only in a Server-client kind of situation where the number/occurrence of client requests cannot be determined/predicted. In this scenario, creating a new Thread each time a client request is made has two dis-advantages:

1. Run time latency for thread creation: As creation of a thread requires some time, thus the actual job doesn’t start as soon as client make a request there can by a delay.
2. Uncontrolled use of System resources: Thread consumes system resources, thus the system may run out of the resources.

Thread Pool addresses aforementioned problems by creating specified number of threads on server start-up instead of creating them during the run time and it limits the number of threads that are running at any given time.

#### Implement Thread Pool in Java

You don’t need to create your own thread pool to manage threads. Java 5 comes with the package java.util.concurrent which provides implementation of Thread pool.

Java thread pool manages the collection of Runnable threads and worker threads execute Runnable from the queue. java.util.concurrent.Executors provide implementation of java.util.concurrent.Executor interface to create the thread pool in java. Let’s write a simple program to explain it’s working.

First we need to have a **Runnable** class, named ThreadProcessor.java
```
package com.hendisantika.thread;

/**
 * Created by IntelliJ IDEA.
 * Project : thread-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-21
 * Time: 14:39
 */
public class ThreadProcessor implements Runnable {

    private int threadCount;

    public ThreadProcessor(int threadCount) {
        this.threadCount = threadCount;
    }

    @Override
    public void run() {
        System.out.println("Number " + threadCount + " is running");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Number " + threadCount + " has been stopped");
    }

}
```

This is **Runnable** class in which there is basic implementation of **run()** method. Following is the Executor class which will create a thread pool and execute above **Runnable** class.

package com.hendisantika.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
```
/**
 * Created by IntelliJ IDEA.
 * Project : thread-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-21
 * Time: 14:45
 */
public class ThreadPoolExample {
    public static void main(String[] ar) {
        ExecutorService executor = Executors.newFixedThreadPool(5);//newFixedThreadPool(5) will create a fixed size thread pool with five working threads

        for (int i = 1; i <= 20; i++) {
            executor.execute(new ThreadProcessor(i));//It will execute the runnable class
        }

        executor.shutdown(); //It will shutdown the executor when all the working threads are done with their jobs

        while (!executor.isTerminated()) {

        }
        System.out.println("Finished all threads");
    }
}
```

In above example, it will create a thread pool with 5 working threads and we are executing 10 jobs in it. It will execute 5 jobs in 5 different threads and other jobs will wait until threads are not free. As soon as a thread gets free, it will pickup a job from the job queue and will perform task.

Following is the output of the above program:
```
Number 1 is running
Number 4 is running
Number 3 is running
Number 2 is running
Number 5 is running
Number 4 has been stopped
Number 2 has been stopped
Number 1 has been stopped
Number 5 has been stopped
Number 8 is running
Number 7 is running
Number 6 is running
Number 3 has been stopped
Number 9 is running
Number 10 is running
Number 8 has been stopped
Number 7 has been stopped
Number 10 has been stopped
Number 13 is running
Number 6 has been stopped
Number 14 is running
Number 9 has been stopped
Number 15 is running
Number 12 is running
Number 11 is running
Number 13 has been stopped
Number 15 has been stopped
Number 12 has been stopped
Number 11 has been stopped
Number 14 has been stopped
Number 19 is running
Number 18 is running
Number 17 is running
Number 16 is running
Number 20 is running
Number 19 has been stopped
Number 17 has been stopped
Number 18 has been stopped
Number 20 has been stopped
Number 16 has been stopped
Finished all threads

Process finished with exit code 0
```

As you can see in the output, it executed 5 task at a time and then pickup next 5 task to execute. This was the basic implementation of Thread pool in java. Read [more](https://www.journaldev.com/1069/threadpoolexecutor-java-thread-pool-example-executorservice) about the thread pool.


References:

https://docs.oracle.com/javase/tutorial/essential/concurrency/pools.html

https://en.wikipedia.org/wiki/Thread_pool

https://www.quora.com/What-are-the-pros-and-cons-of-using-ThreadPool-Vs-creating-our-own-threads

https://stackoverflow.com/questions/3286626/what-is-the-use-of-a-thread-pool-in-java