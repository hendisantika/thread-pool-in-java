package com.hendisantika.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * Project : thread-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-21
 * Time: 14:45
 */
public class ThreadPoolExample {
    public static void main(String[] ar) {
        ExecutorService executor = Executors.newFixedThreadPool(5);//newFixedThreadPool(5) will create a fixed size thread pool with five working threads

        for (int i = 1; i <= 20; i++) {
            executor.execute(new ThreadProcessor(i));//It will execute the runnable class
        }

        executor.shutdown(); //It will shutdown the executor when all the working threads are done with their jobs

        while (!executor.isTerminated()) {

        }
        System.out.println("Finished all threads");
    }
}
