package com.hendisantika.thread;

/**
 * Created by IntelliJ IDEA.
 * Project : thread-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-21
 * Time: 14:39
 */
public class ThreadProcessor implements Runnable {

    private int threadCount;

    public ThreadProcessor(int threadCount) {
        this.threadCount = threadCount;
    }

    @Override
    public void run() {
        System.out.println("Number " + threadCount + " is running");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Number " + threadCount + " has been stopped");
    }

}